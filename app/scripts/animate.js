import { TweenLite } from 'gsap';
import ScrollToPlugin from 'gsap/ScrollToPlugin';

const ANIMATIONS = {
    animScrollTo() {
        const link = document.querySelector('a.content-wrapper__text');
        link.addEventListener('click', function(e) {
            e.preventDefault();
            TweenMax.to(window, 1, {scrollTo:{y:'#tickets', offsetY: 40}});
        });
    },
    moveBigCircle(e) {
        let x = e.touches ? e.touches[0].clientX : e.clientX, 
            y = e.touches ? e.touches[0].clientY : e.clientY;
        let w = window.innerWidth / 2;
        let h = window.innerHeight / 2;

        let l = -(x - w) / (w / 10);
        let t = -(y - h) / (h / 10);

        TweenMax.to('.big-circle', 9, {
            css:{ transform: 'translate('+ l*2 +'px, '+ t*2 +'px)' }
        });
    },
    animBigCircle() {
        window.addEventListener("mousemove", ANIMATIONS.moveBigCircle);
        window.addEventListener("touchstart", ANIMATIONS.moveBigCircle);
        window.addEventListener("touchmove", ANIMATIONS.moveBigCircle);
    },
    init() {
        ANIMATIONS.animScrollTo();
        ANIMATIONS.animBigCircle();
    }
};

module.exports = {
    init : ANIMATIONS.init()
}
