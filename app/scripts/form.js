import isAlpha from 'validator/lib/isAlpha';
import isLength from 'validator/lib/isLength';
import isInt from 'validator/lib/isInt';
import {TweenLite} from 'gsap';

const MESSAGES = {
    clientName : {
        wrong : 'Uzupełnij poprawnie pole!'
    },
    tickets : {
        wrong : 'Uzupełnij poprawnie pole!'
    }
};
const CustomValidator = {
    inputs : null,
    correctInputs : [],
    inputsAmount : 0,
    canSubmit : false,
    engine : {
        checkName(text) {
            if (isLength(text, {min: 3, max: 30})) {
                if(isAlpha(text.replace(/\s/g, ''), 'pl-PL')) {
                    CustomValidator.UIActions.showValidatorMsg('clientName', '.client-name + .error-validation', true);
                    return;
                }
                CustomValidator.UIActions.showValidatorMsg('clientName', '.client-name + .error-validation', false);
                return;
            } 
            CustomValidator.UIActions.showValidatorMsg('clientName', '.client-name + .error-validation', false);
            return;
        },
        checkNumbers(text) {
            if (isInt(text, {min: 0, max: 999})) {
                CustomValidator.UIActions.showValidatorMsg('tickets', '.tickets-amount + .error-validation', true);
                Form.UIActions.refreshPrice(true);
                return;
            }
            CustomValidator.UIActions.showValidatorMsg('tickets', '.tickets-amount + .error-validation', false);
            Form.UIActions.refreshPrice(false);
            return;
        },
        decrementCorrectInputs(element) {
            if(CustomValidator.correctInputs.length > 0) {
                let elementIndex = CustomValidator.correctInputs.indexOf(element);
                if (elementIndex != -1) {
                    CustomValidator.correctInputs.splice(elementIndex, 1);
                }
                CustomValidator.canSubmit = false;
            }
        },
        incrementCorrectInputs(element) {
            if(CustomValidator.correctInputs.length < CustomValidator.inputsAmount) {
                if(CustomValidator.correctInputs.indexOf(element) == -1) {
                    CustomValidator.correctInputs.push(element);
                    if (CustomValidator.correctInputs.length >= CustomValidator.inputsAmount) {
                        CustomValidator.canSubmit = true;
                    }
                }
            } else {
                CustomValidator.canSubmit = true;
            }
        },
        runValidation() {
            CustomValidator.engine.checkName(CustomValidator.inputs[0].value);
            CustomValidator.engine.checkNumbers(CustomValidator.inputs[1].value);
        }
    },
    UIActions : {
        bindValidation() {
            const inputs = document.querySelectorAll('.form__input');
            CustomValidator.inputs = inputs;

            CustomValidator.inputsAmount = inputs.length;

            inputs.forEach(function(element) {
                let validatorAction;
                if(element.classList[1] === 'client-name') {
                    validatorAction = CustomValidator.engine.checkName;
                } else {
                    validatorAction = CustomValidator.engine.checkNumbers;
                }
                element.addEventListener('keyup', function() {
                    setTimeout(function() {
                        validatorAction(element.value);
                    }, 128);
                });
            });
        },
        showValidatorMsg(inputType, selector, state) {
            let msgBox = document.querySelector(selector);
            if (state) {
                if (msgBox.classList.contains('active')) {
                    msgBox.classList.remove('active');
                }
                CustomValidator.engine.incrementCorrectInputs(inputType);
            } else {
                // msgBox.innerHTML = MESSAGES[inputType].wrong;
                if (!msgBox.classList.contains('active')) {
                    msgBox.classList.add('active');
                }
                CustomValidator.engine.decrementCorrectInputs(inputType);
            }
        },
    },
    init() {
        CustomValidator.UIActions.bindValidation();
    }
};

const Form = {
    UIActions : {
        bindSubmitEvent() {
            let submitBtn = document.querySelector('.form__submit');
            submitBtn.addEventListener('click', function(e) {
                e.preventDefault();
                if (CustomValidator.canSubmit) {
                    Form.UIActions.showThankMsg();   
                } else {
                    CustomValidator.engine.runValidation();
                }
            });
        },
        showThankMsg() {
            const thankYouWrapper = document.querySelector('.thank-you-box');
            const formWrapper = document.querySelector('.form-wrapper .form');

            TweenMax.from(formWrapper, 0.3, {opacity: 1});
            TweenMax.to(formWrapper, 0.3, {opacity: 0});

            setTimeout(() => {
                thankYouWrapper.classList.add('active');
                formWrapper.classList.add('hidden');

                TweenMax.from(thankYouWrapper, 0.5, {opacity: 0});
                TweenMax.to(thankYouWrapper, 0.5, {opacity: 1});
            }, 350);
        },
        refreshPrice(state) {
            const price = document.querySelector('.ticket-price').innerHTML;
            const summaryBox = document.querySelector('.price-summary');
            if (state) {
                summaryBox.innerHTML = CustomValidator.inputs[1].value * price;
                TweenMax.from(summaryBox, 0.6, {opacity: .6});
                TweenMax.to(summaryBox, 0.6, {opacity: 1,ease:Power1.easeInOut});
            } else {
                summaryBox.innerHTML = 'XX';
            }
        }
    },
    init() {
        Form.UIActions.bindSubmitEvent();
    }
};

module.exports = {
    initUI : Form.init(),
    validator : {
        init : CustomValidator.init()
    }
}
