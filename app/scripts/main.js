const animations = require('./animate');
const form = require('./form');

document.addEventListener('load', function() {
    animations.init();
    form.validator.init();
    form.initUI();
});